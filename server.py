import base64

import cv2
import zmq
import numpy as np


context = zmq.Context()
footage_socket = context.socket(zmq.PAIR)
footage_socket.connect('tcp://127.0.0.1:5555')


camera = cv2.VideoCapture(0)  # init the camera


while True:
    try:
        grabbed, frame = camera.read()  # grab the current frame
        frame = cv2.resize(frame, (720, 480))# resize the frame
        encoded, buffer = cv2.imencode('.jpg', frame)
        jpg_as_text = base64.b64encode(buffer)
        footage_socket.send(jpg_as_text)
        frame2 = cv2.resize(frame, (180, 120))
        cv2.imshow("Server Self",frame2)
        cv2.waitKey(1)

        frame1 = footage_socket.recv_string()
        img = base64.b64decode(frame1)
        npimg = np.fromstring(img, dtype=np.uint8)
        source = cv2.imdecode(npimg, 1)
        cv2.imshow("Client", source)
        cv2.waitKey(1)

    except KeyboardInterrupt:
        camera.release()
        cv2.destroyAllWindows()
        break