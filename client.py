import cv2
import zmq
import base64
import numpy as np

context = zmq.Context()
footage_socket = context.socket(zmq.PAIR)
footage_socket.bind('tcp://*:5555')
#footage_socket.setsockopt_string(zmq.SUBSCRIBE, np.unicode(''))
camera = cv2.VideoCapture(0)

while True:
    try:
        grabbed, frame1 = camera.read()  # grab the current frame
        frame1 = cv2.resize(frame1, (720, 480))  # resize the frame
        encoded, buffer = cv2.imencode('.jpg', frame1)
        jpg_as_text = base64.b64encode(buffer)
        footage_socket.send(jpg_as_text)
        frame2 = cv2.resize(frame1, (180, 120))
        cv2.imshow("Client Self", frame2)
        cv2.waitKey(1)

        frame = footage_socket.recv_string()
        img = base64.b64decode(frame)
        npimg = np.fromstring(img, dtype=np.uint8)
        source = cv2.imdecode(npimg, 1)

        cv2.imshow(" Server ", source)
        cv2.waitKey(1)

    except KeyboardInterrupt:
        cv2.destroyAllWindows()
        break
